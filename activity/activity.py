# Activity

# 1 Leap Year with Validation. Zero, negative numbers and other data types are not allowed.
try:
    year = int(input("Enter a year: "))
    if year > 0:
        if (year % 400 == 0) and (year % 100 == 0):
            print(f"{year} is a leap year")
        elif (year % 4 == 0) and (year % 100 != 0):
            print(f"{year} is a leap year")
        else:
            print(f"{year} is not a leap year")
    else:
        print("Invalid! Zero or negative numbers are not allowed")
except ValueError:
    print("Invalid. Should enter positive integer data type only")

# 2 Grid with Validation. Zero, negative numbers and other data types are not allowed.
try:
    row = int(input("Enter number of rows: "))
    column = int(input("Enter number of columns: "))
    if row > 0 and column > 0:
        for i in range(row):
            print("*" * column + " ")
    else:
        print("Invalid! Zero or negative numbers are not allowed")
except ValueError:
    print("Invalid. Should enter positive integer data type only")
